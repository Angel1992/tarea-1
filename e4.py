# Asume que ejecutamos las siguientes sentencias de asignación:
#__Autor:_ Angel Bolivar Contento Guamán
#__Email__ angel.b.contento@unl.edu.ec

ancho = 17
alto = 12.0
#Para cada una de las expresiones siguientes, escribe el valor de la expresión y el tipo (del valor de la expresión).
t1 = ancho/2
t2 = ancho/2.0
t3 = alto/3
t4 = 1 + 2 * 5
print (t1)
print (type(t1))
print (t2)
print (type(t2))
print (t3)
print (type(t3))
print (t4)
print (type(t4))